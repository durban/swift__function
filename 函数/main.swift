//
//  main.swift
//  函数
//
//  Created by durban.zhang on 2018/5/6.
//  Copyright © 2018年 durban.zhang. All rights reserved.
//

import Foundation

print("3、Swift函数")


func concat(s1:String, s2:String) -> String {
    return  "\(s1) and \(s2)"
}

func concat1 (s1:String, s2:String, andString3 s3:String) -> String {
    return  "\(s1) and \(s2) and \(s3)"
}

func returnString() -> (String, String, String) {
    return ("Hello1", "Hello2", "Hello3")
}

func functionInFunction() -> Int {
    var m = 10
    func add() {
        m += 1
    }
    
    return m
}

func functionInFunctionTest(num: Int, function: (Int) -> Bool) -> Bool {
    return function(num)
}

func lessThanTen(num: Int) -> Bool {
    if num < 10 {
        return true
    }
    
    return false
}

func echoString() {
    print("输出字符串")
    
    let s = concat(s1: "Hello", s2: "World")
    print(s)
    
    let t = concat1(s1:"Hello", s2:"World", andString3: "String3")
    print(t)
    
    let (s1, s2, s3) = returnString()
    print("\(s1) and \(s2) and \(s3)")
    
    let b1 = functionInFunction()
    let b2 = functionInFunctionTest(num: 60, function: lessThanTen)
    print("b1 = \(b1)")
    print("b2 = \(b2)")
}

echoString()
